This repository documents an experiment to measure how well the benchmarks in [DroidBench](https://github.com/secure-software-engineering/DroidBench) represent real world Android apps in terms of Android APIs involved in the creation of vulnerabilities. Each benchmark comes with a pre-built APK file and the source.

We have used API measure as a metric to measure the representativeness of the DroidBench benchmarks, as described by Ranganath et. al in their [paper](https://arxiv.org/abs/1806.09059). The details of what aspects were considered when measuring representativeness based on API usage can be found [here](https://bitbucket.org/secure-it-i/evaluate-representativeness/src/master/README.md).

## Artifacts

1. The *scripts* folder contains all the scripts required to reproduce the experiment
2. The *output-april-2019* folder contains all the raw files generated from running the scripts
3. The input folder contains the input files required to run the scripts along with a bundle of the DroidBench benchmarks used in this evaluation.
4. The shas for the real-world apps we used can be found [here](https://bitbucket.org/secure-it-i/evaluate-repr-droidbench-jan2019/src/master/input/shas.txt)

## Software Used

1. Ubuntu 18.04 LTS
2. Groovy 2.4.14
3. Android Studio 3.0.1

## Steps to reproduce the experiment

1. Place the sample of real-world apps in *input/androzoo* folder
2. Place the APK files for the DroidBench benchmarks in *input/droidbench-extended* folder
3. If you create a new baseline app then place the corresponding APK file in *input/baseline* folder
4. Set ANDROID_HOME in *scripts/masterRun.sh* and execute it
5. Run *scripts/get-api-real-world-count* followed by *scripts/mergeRealWorldSoFCount.groovy* to generate the number of real-world apps using and the number of Stack Overflow posts discussing APIs used by DroidBench

## Useful Links

1. The [DroidBench bundle](https://bitbucket.org/secure-it-i/evaluate-repr-droidbench-jan2019/src/master/input/DroidBenchExtended.zip) used in this evaluation was downloaded from [ReproDroid](https://foellix.github.io/ReproDroid/).
2. The DroidBench APK files built for and used in this evaluation are available at [DrodiBench-apks](https://bitbucket.org/secure-it-i/evaluate-repr-droidbench-jan2019/src/master/input/droidbench-extended/droidbench-apks-apr2019.zip).
3. The files in *output-april2019/sof-data/* can be reproduced from [StackOverflow-March2019](https://bitbucket.org/secure-it-i/stackoverflow-march2019/src/master/)

## Attribution

Copyright (c) 2019/2018, Kansas State University

Licensed under [BSD 3-clause "New" or "Revised" License](https://choosealicense.com/licenses/bsd-3-clause/)

Contributors:

1. Joydeep Mitra [Creator + Developer]
2. Venkatesh-Prasad Ranganath [Advisor]
