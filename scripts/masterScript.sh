#!/usr/bin/env bash

# Copyright (c) 2018, Kansas State University
#
# BSD 3-clause License
#
# Authors: Venkatesh-Prasad Ranganath
#          Joydeep Mitra

REAL_WORLD_APKS=input/androzoo

OUTPUT_FOLDER=output-april-2019

ANDROID_HOME=~/Android/Sdk
#ANDROID_HOME=~/Library/Android/sdk

echo ""
echo "You will use the following settings:"
echo "  ANDROID_HOME=$ANDROID_HOME"
echo "  OUTPUT_FOLDER=$OUTPUT_FOLDER"
echo "  REAL_WORLD_APKS=$REAL_WORLD_APKS"
read -p "Are these settings correct? (yn) " answer

if [[ $answer != "y" ]] ; then
    echo "Change your setting in scripts/masterRun.sh and rerun it."
    exit
fi
echo ""

mkdir -p $OUTPUT_FOLDER/security

BASELINE_APKS=input/baseline/
BASELINE_ID_API_MAPPING=$OUTPUT_FOLDER/baseline-id-api-mapping.csv
BASELINE_API_PROFILE=$OUTPUT_FOLDER/baseline-api-profile.txt
BASELINE_API_PROFILE_CSV=$OUTPUT_FOLDER/baseline-api-profile.csv
BASELINE_API_PROFILE_LOG=$OUTPUT_FOLDER/baseline-api-profile.log

APIS_TO_IGNORE=$OUTPUT_FOLDER/apis-to-ignore.txt

DROIDBENCH_APKS=/home/joydeep/BenchPress/prepare-benchmarks/DroidBench/ModelReproDroidProject/output-apks
DROIDBENCH_ID_API_MAPPING=$OUTPUT_FOLDER/droidbench-extended-id-api-mapping.csv
DROIDBENCH_API_PROFILE=$OUTPUT_FOLDER/droidbench-extended-api-profile.txt
DROIDBENCH_API_PROFILE_CSV=$OUTPUT_FOLDER/droidbench-extended-api-profile.csv
DROIDBENCH_API_PROFILE_LOG=$OUTPUT_FOLDER/droidbench-extended-api-profile.log

DROIDBENCH_SECURITY_APIS=input/droidbench-security-apis.txt
DROIDBENCH_SECURITY_BENIGN_ID_API_MAPPING=$OUTPUT_FOLDER/security/droidbench-id-api-mapping.csv

REAL_WORLD_ID_API_MAPPING=~/BenchPress/evaluate-repr-ghera-jan2019/output-april-19/real-world-id-api-mapping.csv
REAL_WORLD_API_PROFILE=~/BenchPress/evaluate-repr-ghera-jan2019/output-april-19/real-world-api-profile.txt
REAL_WORLD_API_PROFILE_LOG=$OUTPUT_FOLDER/real-world-api-profile.log
REAL_WORLD_BENIGN_API_PROFILE_CSV=$OUTPUT_FOLDER/real-world-droidbench-api-profile.csv

echo "$(date) Collecting API profile for baseline APKs"
groovy -cp libs/asmdex-1.0.jar:libs/asm-6.0.jar scripts/getAPIProfile.groovy \
    -a $ANDROID_HOME -f $BASELINE_APKS -i $BASELINE_ID_API_MAPPING \
    -p $BASELINE_API_PROFILE > $BASELINE_API_PROFILE_LOG
cat $BASELINE_ID_API_MAPPING | cut -f2 -d'!' > $APIS_TO_IGNORE

echo ""
read -p "Do you want to use input/apis-to-ignore.txt? (yn) " answer

if [[ $answer = "y"  ]] ; then
    cp input/apis-to-ignore.txt $APIS_TO_IGNORE
else
    read -p "Edit $APIS_TO_IGNORE and then press any key."
fi
echo ""


#################################################################
#
# DroidBench section
#
echo "$(date) Collecting API profile for DroidBench APKs"
groovy -cp libs/asmdex-1.0.jar:libs/asm-6.0.jar scripts/getAPIProfile.groovy \
    -a $ANDROID_HOME -f $DROIDBENCH_APKS \
    -i $DROIDBENCH_ID_API_MAPPING -p $DROIDBENCH_API_PROFILE \
    &> $DROIDBENCH_API_PROFILE_LOG

echo "$(date) Converting DroidBench API profile into CSV"
groovy scripts/remapProfile.groovy -s $DROIDBENCH_ID_API_MAPPING \
    -t $DROIDBENCH_ID_API_MAPPING -i $DROIDBENCH_API_PROFILE \
    -o $DROIDBENCH_API_PROFILE_CSV -g $APIS_TO_IGNORE

# #################################################################
# #
# # Real world section
# #
echo "$(date) Collecting API Profile for real world APKs"
groovy -cp libs/asmdex-1.0.jar:libs/asm-6.0.jar scripts/getAPIProfile.groovy \
    -a $ANDROID_HOME -f $REAL_WORLD_APKS \
    -i $REAL_WORLD_ID_API_MAPPING -p $REAL_WORLD_API_PROFILE \
    &> $REAL_WORLD_API_PROFILE_LOG

echo "$(date) Remapping real world API profile into Droidbench API profile"
groovy scripts/remapProfile.groovy -s $REAL_WORLD_ID_API_MAPPING \
    -t $DROIDBENCH_ID_API_MAPPING -i $REAL_WORLD_API_PROFILE \
    -o $REAL_WORLD_BENIGN_API_PROFILE_CSV -g $APIS_TO_IGNORE

# #################################################################
# #
# # Plot all-api proportion graphs
# #
# echo "$(date) Plotting proportions graph"
# Rscript ./scripts/plot_api_usage-23-27.r \
#     $DROIDBENCH_API_PROFILE_CSV $REAL_WORLD_BENIGN_API_PROFILE_CSV \
#     droidbench $OUTPUT_FOLDER

# ##################################################################
# #
# # Security API specific tasks begins here
# #
# echo "$(date) Making id-mapping file for security related apis in benign apps"
# scripts/project-security-id-mapping.sh -i $DROIDBENCH_SECURITY_APIS \
#     -s $DROIDBENCH_ID_API_MAPPING -t $DROIDBENCH_SECURITY_BENIGN_ID_API_MAPPING

# #################################################################
# #
# # Plot security-specific-api proportion graphs
# #
# echo "$(date) Plotting benign security-specific-API proportions graph"
# Rscript ./scripts/plot_api_usage.r \
#     $DROIDBENCH_API_PROFILE_CSV $REAL_WORLD_BENIGN_API_PROFILE_CSV \
#     benign $OUTPUT_FOLDER/security $DROIDBENCH_SECURITY_BENIGN_ID_API_MAPPING
